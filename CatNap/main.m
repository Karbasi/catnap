//
//  main.m
//  CatNap
//
//  Created by Karci Xie on 14-7-20.
//  Copyright (c) 2014年 Karci Xie. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
