//
//  AppDelegate.h
//  CatNap
//
//  Created by Karci Xie on 14-7-20.
//  Copyright (c) 2014年 Karci Xie. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
