//
//  SKSpriteNode+DebugDraw.h
//  CatNap
//
//  Created by Karci Xie on 14-7-21.
//  Copyright (c) 2014年 Karci Xie. All rights reserved.
//

#import <SpriteKit/SpriteKit.h>

@interface SKSpriteNode (DebugDraw)

- (void)attachDebugRectWithSize:(CGSize)s;
- (void)attachDebugFrameFromPath:(CGPathRef)bodyPath;

@end
